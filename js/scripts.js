function ibg() {
  $.each($('._ibg'), function (index, val) {
    if ($(this).find('img').length > 0) {
      $(this).css('background-image', 'url("' + $(this).find('img').attr('src') + '")')
    }
  })
}
ibg()

// ? <main page slider >========================================================================
$(document).ready((e) => {
  var currentSlide = '0'
  $('.mini-slider__slide').each(function (i) {
    $(this).attr('id', `slide-${i}`)
  })

  $('.mini-navigator__item').each(function (i) {
    $(this).attr('nav', i)
  })

  $('.mini-slider__slide')[0].classList.add('active')
  $('.mini-navigator__item')[0].classList.add('active')

  $('.mini-navigator__item').click(function (event) {
    if ($(this).attr('nav') !== currentSlide) {
      $('.mini-navigator__item, .mini-slider__slide').removeClass('active')
      $(this).addClass('active')
      setTimeout(() => {
        $('#slide-' + currentSlide).removeClass('hide')
        currentSlide = $(this).attr('nav')
      }, 500)
      $('#slide-' + currentSlide).addClass('hide')
      $('#slide-' + $(this).attr('nav')).addClass('active')
    }
  })

  var bigSlideCurrent = '0'
  $('.slide-nav__item').each(function (i) {
    $(this).attr('navigate', i)
  })

  $('.mini-slider').each(function (i) {
    $(this).attr('id', `bigSlide-${i}`)
  })

  $('.slide-nav__item').click(function (e) {
    if ($(this).attr('navigate') !== bigSlideCurrent) {
      $('.slide-nav__item, .main-slider__slide').removeClass('active')
      $(this).addClass('active')

      $('#bigSlide-' + bigSlideCurrent).addClass('hide')
      updateSlide($(this).attr('navigate'))
      setTimeout(() => {
        $('#bigSlide-' + bigSlideCurrent).removeClass('hide')
        bigSlideCurrent = $(this).attr('navigate')
      }, 700)

      $('#bigSlide-' + $(this).attr('navigate')).addClass('active')
    }
  })

  let updateSlide = (val) => {
    $('.mini-navigator__item, .mini-slider__slide').removeClass('active')

    document.getElementsByClassName('main-slider__slide')[parseInt(val)].getElementsByClassName('mini-slider__slide')[0].classList.add('active')
    document.getElementsByClassName('main-slider__slide')[parseInt(val)].getElementsByClassName('mini-navigator__item')[0].classList.add('active')
    currentSlide = document.getElementsByClassName('main-slider__slide')[parseInt(val)].getElementsByClassName('mini-navigator__item')[0].attributes
      .nav.value
    console.log(
      document.getElementsByClassName('main-slider__slide')[parseInt(val)].getElementsByClassName('mini-navigator__item')[0].attributes.nav.value
    )
  }
})

//? lang========================================================================================

$(document).ready((e) => {
  $('.lang__dropdown>ul>li').click(function (e) {
    $('.lang__dropdown>ul>li').removeClass('active')
    $(this).addClass('active')
    $('#current-lang')[0].innerText = $(this)[0].innerText
  })
  $('.lang__current-title').click(function (e) {
    $(this).toggleClass('active')
    $('.lang__dropdown').toggleClass('active')
  })
})
window.addEventListener('click', function (e) {
  if (
    !($('.lang__dropdown')[0].contains(e.target) || $('.lang__current-title')[0].contains(e.target)) ||
    $('.lang__dropdown')[0].contains(e.target)
  ) {
    $('.lang__current-title, .lang__dropdown').removeClass('active')
  }
})
//<SEARCH>============================================================================================================
function openSearch() {
  $('.search').addClass('active')
  $('body').addClass('active')
}

$(document).mouseup(function (e) {
  if ($(e.target).closest('.search').length === 0) {
    $('.search').removeClass('active')
    $('body').removeClass('active')
  }
})
//</SEARCH>============================================================================================================

$(document).ready(function () {
  $('.menu__burger').click(function (event) {
    $('.menu__burger, .menu__body, .wrapper').toggleClass('__active')
    $('body').toggleClass('_lock')
  })
})

$(window).on('scroll', function () {
  if ($(window).scrollTop()) {
    $('.header').addClass('__fixed')
  } else {
    $('.header').removeClass('__fixed')
  }
})
